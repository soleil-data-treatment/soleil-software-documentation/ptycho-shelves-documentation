# Aide et ressources de ptycho-shelves pour Synchrotron SOLEIL

[<img src="https://www.psi.ch/sites/default/files/styles/primer_full_xl/public/2020-05/ptycho_shelves_logo_small.png?itok=sam8Rdjx" width="150"/>](https://www.psi.ch/en/sls/csaxs/software)

## Résumé

- ptychographie et ptychotomographie
- Open source

## Sources

- Code source:  à demander avec les dévs
- Documentation officielle: https://www.psi.ch/en/media/64645/download

## Navigation rapide

| Ressources annexes |
| - |
| [Guides d'utilisateurs](https://journals.iucr.org/j/issues/2020/02/00/zy5001/index.html) |

## Installation

- Systèmes d'exploitation supportés: Windows,  à vérifier sur machine virtuelle
- Installation: Je ne sais pas,  pas nécessaire de l'installer,  pas de distribution

## Format de données

- en entrée: hdf5
- en sortie: tiff,  format matlab
- sur la Ruche
